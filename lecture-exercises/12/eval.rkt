#lang racket

;; A Value is one of
;; - (r:number Real)
;; A Variable is (r:variable Symbol)
(struct r:number (value) #:transparent)

;; An Expression is one of
;; - Value
;; - (r:variable Symbol)
;; - (r:apply Expression (Listof Expression))
(struct r:variable (name) #:transparent)
(struct r:apply (func args) #:transparent)

;; ----------------------------------------

;; r:eval-exp : Expr -> (Racket values)
;; NOTE: limited (for now) to binary functions!
(define (r:eval-exp expr)
  (match expr
    [(r:number num) num]
    [(r:variable name)
     (r:eval-builtin name)]
    [(r:apply func (list arg1 arg2))
     (define func-val (r:eval-exp func))
     (define arg1-val (r:eval-exp arg1))
     (define arg2-val (r:eval-exp arg2))
     (func-val arg1-val arg2-val)]))

;; r:eval-builtin : Symbol -> (Racket binary function)
(define (r:eval-builtin name)
  (cond [(equal? name '+) +]
        [(equal? name '-) -]
        [(equal? name '*) *]
        [else (error 'r:eval-builtin "unknown variable: ~e" name)]))

(require rackunit)
(check-equal? (r:eval-exp (r:number 5)) 5)
(check-equal? (r:eval-exp (r:number 10)) 10)
(check-equal? (r:eval-exp (r:variable '+)) +)
(check-equal? (r:eval-exp
               (r:apply
                (r:variable '+)
                (list (r:number 10) (r:number 5))))
               15)
