#lang typed/racket

(: circumference : Real -> Real)
(define (circumference radius)
  (* 2 pi radius))

(struct point ([x : Real] [y : Real]) #:transparent)

(: distance : point -> Real)
(define (distance p)
  (match p
    [(point x y)
     (sqrt (+ (sqr x) (sqr y)))]))
