#lang racket

(define (f x y)
  (lambda (b)
    (cond [b x] [else y])))
(define g (f 1 2))
g

;; (pair-left (pair L R)) = L
