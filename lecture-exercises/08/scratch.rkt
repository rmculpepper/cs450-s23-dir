#lang racket


;; (join "," (list "x" "y" "z")) ;; "x,y,z"


;; if we've gotten to (list "z") from the original list,
;; what do we need for the accumulator?

;; it needs to contain the "partial answer" for the elements
;; that you've already seen

"x,y"
