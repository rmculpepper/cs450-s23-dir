#lang racket

;; if : Boolean X X -> X
(define (if b then-branch else-branch)
  (cond [b then-branch] [else else-branch]))

;; factorial : Nat -> Nat
(define (factorial n)
  (if (= n 0) 1 (* n (factorial (- n 1)))))

(factorial 10)
