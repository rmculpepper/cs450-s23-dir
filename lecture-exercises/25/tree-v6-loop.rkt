#lang racket
(require "stack-magic.rkt")
(provide (all-defined-out))

(define-syntax-rule (while condition body ...)
  (let loop () (when condition body ... (loop))))

;; ----------------------------------------

;; A BT (binary tree) is one of
;; - #f
;; - (node Real BT BT)
(struct node (value left right) #:transparent)

;; A (Continuation X Z) is one of
;; - '()    -- if X = Z
;; - (cons (Continue X Y) (Continuation Y Z))

;; A (Continue BT BT) is one of
;; - (continue-of-left BT Real)
;; - (continue-of-right Real BT)
(struct continue-of-left (right new-n) #:transparent)
(struct continue-of-right (new-n new-left) #:transparent)

;; path-sum-tree : BT -> BT
;; Replaces each node's value with the cumulative sum of the path from the root
;; down to that node.
(define (path-sum-tree t0)
  ;; Variables
  (define t t0)  ;; t in path-sum-tree-m; x in apply-continuation, apply-continue
  (define acc 0) ;; acc in path-sum-tree-m; unused by apply-continuation
  (define k '())

  (define continue-outer-loop? #t)
  (while (and continue-outer-loop? (or t (pair? k)))
    ;; path-sum-tree-m loop
    (while t
      (match t
        [(node n left right)
         (set! acc (+ n acc))
         (set! k (cons (continue-of-left right acc) k))
         (set! t left)]))

    ;; apply-continuation loop
    (define continue-ac-loop? #t)
    (while continue-ac-loop?
      (match k
        ['()
         (set! continue-ac-loop? #f)
         (set! continue-outer-loop? #f)]
        [(cons continue next-k)
         (set! k next-k)
         ;; apply-continue
         (match continue
           [(continue-of-left right new-n)
            (define new-left t)
            (set! k (cons (continue-of-right new-n new-left) k))
            (set! t right)
            (set! acc new-n)
            ;; break the apply-continuation loop, go back to path-sum-tree-m loop
            (set! continue-ac-loop? #f)]
           [(continue-of-right new-n new-left)
            (define new-right t)
            (set! t (node new-n new-left new-right))])])))

  t)

;; Example:
(equal? (path-sum-tree
         (node 5
               (node 2 #f #f)
               (node 3 #f (node 10 #f #f))))
        (node 5
              (node 7 #f #f)
              (node 8 #f (node 18 #f #F))))

(define (deep-tree n)
  (for/fold ([t #f]) ([i (in-range n)]) (node i t #f)))
(define tree1000 (deep-tree 1000))
(void (path-sum-tree tree1000))
