#lang typed/racket

;; The List monad

(: pure (All [X] X -> (Listof X)))
(define (pure v) (list v))

(: bind (All [X Y] (Listof X) (X -> (Listof Y)) -> (Listof Y)))
(define (bind xs f)
  (append* (map f xs)))

;; Doesn't typecheck because of TR weirdness, but append* is built in.
;; (: append* (All [X] (Listof (Listof X)) -> (Listof X)))
;; (define (append* xss) (foldr append '() xss))

(define-syntax do
  (syntax-rules (: <-)
    [(do expr)
     expr]
    [(do var : type <- rhs rest ...)
     (bind rhs (lambda ([var : type]) (do rest ...)))]))

;; ----------------------------------------

#;
(do
    x : Integer <- (list 1 2 3)
    y : Integer <- (list 10 20 30)
    z : Void <- (if (not (zero? (modulo (+ x y) 11)))
                    (pure (void))
                    '())
    (pure (+ x y)))

;; ----------------------------------------

(: assert (Boolean -> (Listof Void)))
(define (assert b)
  (if b (pure (void)) '()))

(do
    x : Integer <- (range 2 100)
    y : Integer <- (range x 100)
    _a : Void <- (assert (= (* x y) 54))
  ;; _b : Void <- (assert (<= x y))
    (pure (cons x y)))

#|

> (do xs : (Listof Integer) <- (list (list 1 2) (list 10 20))
      x : Integer <- xs
      y : Integer <- xs
    (pure (+ x y)))
- : (Listof Integer)
'(2 3 3 4 20 30 30 40)
> (do xs : (Listof Integer) <- (list (list 1 2) (list 10 20))
      x : Integer <- xs
      y : Integer <- xs
    (pure (cons x y)))
- : (Listof (Pairof Integer Integer))
'((1 . 1) (1 . 2) (2 . 1) (2 . 2) (10 . 10) (10 . 20) (20 . 10) (20 . 20))

|#
