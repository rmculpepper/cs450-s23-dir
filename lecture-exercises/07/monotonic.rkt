#lang racket

;; double : Real -> Real
(define (double n) (* 2 n))

;; monotonic : (Real -> Real) Real -> Boolean
(define (monotonic? f x)
  (>= (f x) x))

;; Tests
(require rackunit)
(check-true (monotonic? double 3))
(check-false (monotonic? (lambda (x) (- x 1)) 3))